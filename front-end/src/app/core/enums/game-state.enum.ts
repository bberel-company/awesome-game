export enum GameState {
  START = 1,
  PLAYING = 2,
  OVER = 3,
}
