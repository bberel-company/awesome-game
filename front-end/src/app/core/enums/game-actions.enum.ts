export enum GameActions {
  attack = 'playerAttack',
  heavyAttack = 'playerHeavyAttack',
  heal = 'heal',
  leaveCombat = 'leaveCombat',
  monsterAttack = 'monsterAttack',
  monsterHeavyAttack = 'monsterHeavyAttack',
  attackMissed = 'attackMissed'
}

export enum GameActionsDescription {
  playerAttack = 'Normal Attack',
  playerHeavyAttack = 'Heavy Attack',
  heal = 'Healing Potion',
  leaveCombat = 'Left the Combat',
  monsterAttack = 'Normal Attack',
  monsterHeavyAttack = 'Heavy Attack',
  attackMissed = ''
}
