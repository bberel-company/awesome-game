import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Player } from '../models/player.model';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private endpoint = `${environment.apiBasePath}/players`;

  constructor(
    private http: HttpClient
  ) { }

  getPlayerList() {
    const url = `${this.endpoint}`;
    return this.http.get<Player[]>(url)
      .pipe(
        take(1));
  }

  postPlayer(player: Player) {
    const url = `${this.endpoint}`;
    return this.http.post<Player>(url, player)
      .pipe(
        take(1));
  }

}
