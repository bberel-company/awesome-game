import { Injectable } from '@angular/core';
import { GameActions, GameActionsDescription } from '../enums/game-actions.enum';
import { GameState } from '../enums/game-state.enum';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  game = {
    state: GameState.START,
    currentRound: 0,
    unities: [
      {
        name: 'Player',
        type: 'player',
        hp: 100,
        heavyAttackTurnsRemaining: 0,
        heavyAttackIsOnCd: false,
        receivedHeavyAttack: false,
        lifeColor: 'unit-success',
        leftCombat: false,
        score: 0
      },
      {
        name: 'Monster',
        type: 'monster',
        hp: 100,
        heavyAttackTurnsRemaining: 0,
        heavyAttackIsOnCd: false,
        receivedHeavyAttack: false,
        lifeColor: 'unit-success',
        score: 0
      }
    ],
    combatLog: <any>[],
    result: <any>null
  }

  constructor() { }

  startNewGame = () => {
    // if (this.checkGameSate()) {
    if (this.game.state === GameState.START || this.game.state === GameState.OVER) {
      this.game.state = GameState.PLAYING;
      this.game.currentRound = 1;
      this.game.unities = [
        {
          name: 'Player',
          type: 'player',
          hp: 100,
          heavyAttackTurnsRemaining: 0,
          heavyAttackIsOnCd: false,
          receivedHeavyAttack: false,
          lifeColor: 'unit-success',
          leftCombat: false,
          score: 0
        },
        {
          name: 'Monster',
          type: 'monster',
          hp: 100,
          heavyAttackTurnsRemaining: 0,
          heavyAttackIsOnCd: false,
          receivedHeavyAttack: false,
          lifeColor: 'unit-success',
          score: 0
        }
      ];
      this.game.combatLog = [];
      this.render();
    }
    // }
  }

  public clearBattleground() {
    this.game.state = GameState.START;
  }

  public update(action: GameActions, attacker: any, target: any) {
    if (action === GameActions.monsterAttack) {
      return;
    }

    if (action === GameActions.leaveCombat) {
      this.leaveCombat(action, attacker);
    }

    if (this.checkGameSate() === GameState.PLAYING) {
      this[action](action, attacker, target, 0);

      this.monsterTurn(GameActions.monsterAttack, this.game.unities[1], this.game.unities[0]);

      this.game.currentRound++;
    }

    if (this.checkGameSate() === GameState.OVER) {
      this.gameResult();
    }

    this.render();
  }

  public monsterTurn(action: GameActions, attacker: any, target: any) {
    if (attacker.hp <= 0) {
      return;
    }

    if (attacker.receivedHeavyAttack && this.checkMissChance()) {
      this.attackMissed(action, attacker, target, 0);
    } else if (!attacker.heavyAttackIsOnCd) {
      this.monsterHeavyAttack(GameActions.monsterHeavyAttack, attacker, target)
    } else {
      this.monsterAttack(GameActions.monsterAttack, attacker, target)
    }
  }

  public attack(action: GameActions, attacker: any, target: any, damage: number) {
    const damageDone = (target.hp - damage <= 0) ? damage - ((target.hp - damage) * -1) : damage;
    target.hp -= damageDone;

    this.damageDone(action, attacker, target, damageDone);
    target.receivedHeavyAttack = false;

    this.reduceHeavyAttackCd(attacker);
  }

  public playerAttack(action: GameActions, attacker: any, target: any) {
    const damage = this.generateValue(5, 10);
    this.attack(action, attacker, target, damage);
  }

  public monsterAttack(action: GameActions, attacker: any, target: any) {
    const damage = this.generateValue(6, 12);
    this.attack(action, attacker, target, damage);
  }

  public heavyAttack(action: GameActions, attacker: any, target: any, damage: number) {
    if (!attacker.heavyAttackIsOnCd) {
      const damageDone = (target.hp - damage <= 0) ? damage - ((target.hp - damage) * -1) : damage;
  
      attacker.heavyAttackTurnsRemaining = 2;
      attacker.heavyAttackIsOnCd = true;
      target.hp -= damageDone
      target.receivedHeavyAttack = true;

      this.damageDone(action, attacker, target, damageDone);
    }
  }

  public playerHeavyAttack(action: GameActions, attacker: any, target: any) {
    const damage = this.generateValue(10, 20);
    this.heavyAttack(action, attacker, target, damage);
  }

  public monsterHeavyAttack(action: GameActions, attacker: any, target: any) {
    const damage = this.generateValue(8, 16);
    this.heavyAttack(action, attacker, target, damage);

    attacker.heavyAttackTurnsRemaining = 3;
  }

  public heal(action: GameActions, attacker: any, target: any) {
    const heal = this.generateValue(5, 15);
    const healDone = (target.hp + heal >= 100) ? heal - (target.hp + heal - 100) : heal;

    target.hp += healDone;

    this.healDone(action, attacker, target, healDone);
    
    this.reduceHeavyAttackCd(attacker);
  }

  public leaveCombat(action: GameActions, attacker: any) {
    this.game.state = GameState.OVER;
    attacker.leftCombat = true;
    this.leftCombat(action, attacker);
  }

  public gameResult() {
    const r = this.game.unities.reduce((a, b) => {
      if (a.leftCombat) {
        return b;
      } else if (b.leftCombat) {
        return a;
      } else {
        return a.hp > b.hp ? a : b;
      }
    });
    r.score = this.scoreResult(r.hp, this.game.currentRound);
    console.log(r);
    this.game.result = r;
  }

  public scoreResult(hp: number, round: number) {
    return Math.ceil((hp * 1000) / round);
  }

  public render() {
    this.game.unities.forEach(u => {
      if (u.hp <= 20) {
        u.lifeColor = 'unit-danger';
      } else if (u.hp <= 50 && u.hp > 20) {
        u.lifeColor = 'unit-alert';
      } else {
        u.lifeColor = 'unit-success';
      }
    });
  }

  public reduceHeavyAttackCd(attacker: any) {
    if (attacker.heavyAttackIsOnCd) {
      attacker.heavyAttackTurnsRemaining--;
      if (attacker.heavyAttackTurnsRemaining === 0) {
        attacker.heavyAttackIsOnCd = false;
      }
    }
  }

  public generateValue = (min: number, max: number) => {
    const _min = Math.ceil(min);
    const _max = Math.ceil(max);
    return Math.floor(Math.random() * (_max - _min + 1)) + _min;
  }

  public checkGameSate() {
    if (!this.game.unities.every(u => u.hp > 0)) {
      this.game.state = GameState.OVER;
    }

    return this.game.state;
  }

  public checkHealChance() {
    return (Math.floor(Math.random() * (100 - 0 + 1)) + 0) <= 25;
  }

  public checkMissChance() {
    return (Math.floor(Math.random() * (100 - 0 + 1)) + 0) <= 50;
  }

  private attackMissed(action: GameActions, attacker: any, target: any, damage: number) {
    const message = `${attacker.name} missed attack (${damage}) to ${target.name} when used ${GameActionsDescription[action]}`;
    this.log(GameActions.attackMissed, attacker, target, damage, message);
  }

  private damageDone(action: GameActions, attacker: any, target: any, damage: number) {
    const message = `${attacker.name} caused damage (${damage}) to ${target.name} using ${GameActionsDescription[action]}`;
    this.log(action, attacker, target, damage, message);
  }

  private healDone(action: GameActions, attacker: any, target: any, heal: number) {
    const message = `${attacker.name} healed (${heal}) to ${target.name} when used ${GameActionsDescription[action]}`;
    this.log(GameActions.heal, attacker, target, heal, message);
  }

  private leftCombat(action: GameActions, attacker: any) {
    const message = `${attacker.name} left the combat on round ${this.game.currentRound}`;
    this.log(GameActions.leaveCombat, attacker, attacker, 0, message);
  }

  private log(action: GameActions, attacker: any, target: any, value: number, message: string) {
    const log = {
      type: action,
      from: attacker.name,
      to: target.name,
      value,
      message
    }
    this.game.combatLog.push(log);
  }

}
