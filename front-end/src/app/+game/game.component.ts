import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { GameActions } from '../core/enums/game-actions.enum';
import { GameState } from '../core/enums/game-state.enum';
import { GameService } from '../core/services/game.service';
import { PlayerService } from '../core/services/player.service';

@Component({
  selector: 'game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  title = 'Game Component';

  public gameState = GameState;
  public gameActions = GameActions;

  public form = this.formBuilder.group({
    playerName: [null, [Validators.required, Validators.maxLength(15)]],
    score: [null, [Validators.required]],
    data: [null, [Validators.required]]
  });

  get game() { return this.gameService.game; };
  get log() { return this.game.combatLog.reverse(); };
  get player() { return this.game.unities[0]; };
  get monster() { return this.game.unities[1]; };
  get result() { return this.game.result; };

  constructor(
    private playerService: PlayerService,
    private gameService: GameService,
    private formBuilder: UntypedFormBuilder
  ) { }

  ngOnInit() { }

  startNewGame = () => {
    this.gameService.startNewGame();
  }

  cast(action: GameActions) {
    this.gameService.update(action, this.game.unities[0], this.game.unities[1]);
  }

  castSelfHeal() {
    this.gameService.update(this.gameActions.heal, this.game.unities[0], this.game.unities[0]);
  }

  saveMatch() {
    const data = Date.now();
    this.form.patchValue({ score: this.result.score, data })
    if (this.form.valid) {
      console.log(this.form.value);
      this.playerService.postPlayer(this.form.value)
        .subscribe(res => {
          this.gameService.clearBattleground();
          console.log(res)
        })
      this.form.setValue({ playerName: null, score: null, data: null });
    }
  }
}
