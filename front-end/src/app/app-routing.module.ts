import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './+game/game.component';
import { HomeComponent } from './+home/home.component';
import { RankingComponent } from './+ranking/ranking.component';
import { LayoutComponent } from './core/components/layout/layout.component';

const routes: Routes = [
  {
    path: 'home', component: LayoutComponent,
    children: [{ path: '', component: HomeComponent }]
  },
  {
    path: 'ranking', component: LayoutComponent,
    children: [{ path: '', component: RankingComponent }]
  },
  {
    path: 'game', component: LayoutComponent,
    children: [{ path: '', component: GameComponent }]
  },
  { path: '**', redirectTo: 'home' },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
