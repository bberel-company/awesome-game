import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../core/services/player.service';

@Component({
  selector: 'ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {
  title = 'Ranking Component';

  players = this.playerService.getPlayerList();

  constructor(private playerService: PlayerService) { }

  ngOnInit() { }

}
