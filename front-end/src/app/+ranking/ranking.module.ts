import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RankingComponent } from './ranking.component';


@NgModule({
  declarations: [
    RankingComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: []
})
export class RankingModule { }
