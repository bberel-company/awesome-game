# Awesome Game

Jogo desenvolvido como desafio do processo de selecao desenvolvedor FPF

## Instrucoes

abra um terminal e navegue ate a pasta do projeto

> para instalar o back-end

navegue ate o diretorio back-end ``cd back-end``
 
instale as dependencias do projeto ``npm install``
 
execute a aplicacao ``npm start`` ou ``npm run watch`` para acompanhar as modicacoes realizadas no codigo
 
> para instalar o front-end

em outro terminal navegue ate o diretorio front-end ``cd front-end``

instale as dependencias do projeto ``npm install``

execute a aplicacao ``npm start``

abra o app em http://localhost:4200
