const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('config');

module.exports = () => {
  const app = express();

  // Settings
  app.set('port', process.env.PORT || config.get('server.port'));

  // Middlewares
  app.use(cors());
  app.use(bodyParser.json());

  return app;
}