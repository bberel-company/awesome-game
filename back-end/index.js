const express = require('./config/express')();
const port = express.get('port');

const routes = require('./app/routes');

// Main application
express.use(routes);

// Server instance
express.listen(port, () => {
  console.log(`Running on port ${port}`);
  console.log(`Open http://localhost:${port}`);
});