const router = require('express-promise-router')();

const app = require('../controllers/app');
const players = require('../controllers/players');

router.use('/api/players', players);
router.use('/', app);

module.exports = router;