/* const router = require("express-promise-router")();

router.get('/', (req, res, next) => {
  const api = {
    '/': 'Raiz da api e lista os endpoints disponiveis',
    '/players': {
      methods: ['GET', 'POST'],
      endpoints: [
        {
          type: 'GET',
          description: 'lista de jogadores que venceram um mostro'
        },
        {
          type: 'POST',
          description: 'adiciona um jogador para a lista dos vencedores'
        }
      ]
    },
  };

  res.send(api);
});

module.exports = router; */

module.exports = (req, res, next) => {
  const api = {
    '/': 'Raiz da api e lista os endpoints disponiveis',
    '/api/players': {
      methods: ['GET', 'POST'],
      endpoints: [
        {
          type: 'GET',
          description: 'lista de jogadores que venceram um mostro'
        },
        {
          type: 'POST',
          description: 'adiciona um jogador para a lista dos vencedores'
        }
      ]
    },
  };

  res.send(api);
};
