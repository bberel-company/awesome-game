const router = require("express-promise-router")();

router.get('/', (req, res, next) => {
  let players = require('../data/players.json').players.data;
  players = players.sort((a, b) => b.score - a.score);
  res.send(players);
});

router.post('/', (req, res, next) => {
  let players = require('../data/players.json').players.data;
  players.push({
    playerName: req.body.playerName,
    score: req.body.score,
    data: req.body.data
  })
  res.send(true);
});

module.exports = router;